<!DOCTYPE html>
<html>
   <head>
      <title>Berlatih HTML</title>
   </head>
   <body>
      <h1>Buat Account Baru!</h1>
      <h2>Sign Up Form</h2>
      <table border = "0px">
         <form method="post" action="{{ url('/welcome') }}">
         @csrf
            <tr>
               <td>First name :</td>
            </tr>
            <tr>
               <td><input type ="text" name="fname"></td>
            </tr>
            <tr>
               <td>Last name :</td>
            </tr>
            <tr>
               <td><input type ="text" name="lname"></td>
            </tr>
            <tr>
               <td>Gender :
            </tr>
            <tr>
               <td><input type ="radio" name="gender" value="Male">
                  <label>Male</label>
               </td>
            </tr>
            <tr>
               <td><input type ="radio" name="gender" value="Female">
                  <label>Female</label>
               </td>
            </tr>
            <tr>
               <td><input type ="radio" name="gender" value="Other">
                  <label>Other</label>
               </td>
            </tr>
            <tr>
               <td>Nationality : </td>
            </tr>
            <tr>
               <td>
                  <select name="nationality">
                     <option value="indonesian">indonesian</option>
                     <option value="singapore">singapore</option>
                     <option value="malaysian">malaysian</option>
                  </select>
               </td>
            </tr>
            <tr>
               <td>Language Spoken : </td>
            </tr>
            <tr>
               <td><input type ="checkbox" name="language" value="Bahasa Indonesia">
                  <label>Bahasa Indonesia</label>
               </td>
            </tr>
            <tr>
               <td>
                  <input type ="checkbox" name="language" value="English">
                  <label>
                     English</label>
               </td>
            </tr>
            <tr>
            <td><input type ="checkbox" name="language" value="Other">
            <label>Other</label></td>
            </tr>
            <tr>
               <td>Bio :</td>
            </tr>
            <tr>
               <td><textarea rows="6" cols="30" name="bio"></textarea></td>
            </tr>
            <tr>
               <td><input type="submit" value="Sign Up"></td>
            </tr>
         </form>
      </table>
   </body>
</html>